import numpy as np
import pandas as pd
from activation_functions import degrau


from perceptron import Perceptron, std_error
from sklearn.metrics import precision_score
from sklearn.preprocessing import StandardScaler

def import_data():
    train = np.array(pd.read_csv('datasets/prova1.2/train.csv'))
    train_x = train[:, :3]
    train_y = train[:, 3]

    test_x = np.array(pd.read_csv('datasets/prova1.2/teste.csv'))

    return train_x, train_y, test_x



if __name__ == '__main__':
    

    train_x, train_y, test_x = import_data()

    # É necessário normalizar os dados por serem grandes demais para o cálculo do erro
    scaler = StandardScaler()
    train_x_scaled = scaler.fit_transform(train_x)
    test_x_scaled = scaler.fit_transform(test_x)

    adaline = Perceptron(3, degrau, None, std_error)
    adaline.fit_adaline(10000, 0.01, train_x_scaled, train_y)

    prediction = adaline.predict(train_x_scaled)

    print('Precisão treino: ', precision_score(train_y, prediction))

    print(adaline.predict(test_x_scaled))
    # Resultados testes:
    #
    # [-1 -1  1  1  1  1 -1 -1  1 -1]
    # [-1 -1  1  1  1  1 -1 -1  1 -1]
    # [-1 -1  1  1  1  1 -1 -1  1 -1]

    
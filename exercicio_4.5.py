from perceptron import Perceptron, quadratic_error, quadratic_error_dx, std_error
from activation_functions import degrau, linear, gen_tanh

import numpy as np
import pandas as pd
from sklearn.metrics import precision_score

import matplotlib.pyplot as plt

if __name__ == '__main__':

    adaline = Perceptron(4, degrau, None, std_error)
    tanh, tanh_dx = gen_tanh(1)
    perceptron = Perceptron(4, tanh, tanh_dx)
    print('Pesos iniciais:')
    print(perceptron.weights)

    dataset_treino = np.array(pd.read_csv(r'datasets/exercicio_cap_4/dataset_treino.csv'))
    train_x = dataset_treino[:, :4]
    train_y = dataset_treino[:, 4]

    test_x = np.array(pd.read_csv(r'datasets/exercicio_cap_4/dataset_teste.csv'))

    # input(train_y.shape)
    # print(train_x)
    # print(train_y)
    # input()

    adaline.fit_adaline(10000, 0.001, train_x, train_y)
    # adaline.fit_adaline(3000, 0.0025, train_x, train_y)
    # adaline.fit_adaline(1000, 0.01, train_x, train_y)


    n_epochs = 130
    epocas, erro = perceptron.fit(n_epochs, 0.0025, train_x, train_y)
    print('epocas = ', epocas)
    print('Pesos finais')
    print(perceptron.weights)

    print(degrau(perceptron.predict(test_x)))
    plt.plot(np.linspace(0, epocas+1, epocas+1), erro)
    plt.ylabel('Erro quadrático', fontsize=18)
    plt.xlabel('Épocas', fontsize=18)
    plt.show()


    print('Precisão: ', precision_score(train_y, adaline.predict(train_x)))
    # print('Precisão perceptron: ', precision_score(train_y, degrau(perceptron.predict(train_x))))

    errados = train_x[train_y != degrau(adaline.predict(train_x)), :]
    errados_y = train_y[train_y != degrau(adaline.predict(train_x))]
    print(errados)
    print(adaline.predict_adaline(errados))
    print(std_error(adaline.predict_adaline(errados), errados_y))
    print(quadratic_error(adaline.predict_adaline(errados), errados_y))
    print(errados_y)





    # adaline.fit_adaline(201, 0.0001, errados, errados_y)

    # print('\n\nTreinei de novo\n\n')

    # errados = train_x[train_y != degrau(adaline.predict(train_x)), :]
    # errados_y = train_y[train_y != degrau(adaline.predict(train_x))]

    # print(errados)
    # print(adaline.predict_adaline(errados))
    # print(std_error(adaline.predict_adaline(errados), errados_y))
    # print(quadratic_error(adaline.predict_adaline(errados), errados_y))
    # print(errados_y)

    # errados = train_x[train_y != degrau(perceptron.predict(train_x)), :]
    # print(train_x[train_y != degrau(perceptron.predict(train_x)), :])
    # print(perceptron.predict_adaline(errados))
    

    # print('Erro final: ', np.sum(np.abs(adaline.error(adaline.predict(train_x), train_y))))

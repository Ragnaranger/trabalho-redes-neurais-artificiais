import numpy as np
import pandas as pd

class Kohonen:

    def __init__(self, rows, cols, n_features):
        self.n_rows = rows
        self.n_cols = cols
        self.n_features = n_features
        self.map = np.random.random((rows, cols, n_features)) - 0.5
        
        # print('Shape mapa: ', self.map.shape)


    def fit(self, train_x:np.ndarray, learn_rate=0.2, n_epochs=1000):

        # Para cada época...
        for epoch in range(n_epochs):
            # Para cada amostra no treinamento...
            for x in train_x:
                # Achar o que tem menor distância
                # distancias = np.linalg.norm(self.map - x.reshape((1, 1, -1)), 2, axis=2) # Método alternativo
                x = x.reshape(1, 1, -1)
                distancias = np.sqrt(np.sum(np.power(self.map - x, 2), axis=2))
                i, j = np.unravel_index(distancias.argmin(), distancias.shape)                

                # Ajustar pesos
                self.map[i, j, :] = self.map[i, j, :] + learn_rate*(x - self.map[i, j, :])

                # Ajustar vizinhos com learn rate menor
                if not(i == 0):
                    self.map[i-1, j, :] = self.map[i-1, j, :] + (learn_rate/2)*(x - self.map[i-1, j, :])
                if not(i == self.map.shape[0]-1):
                    self.map[i+1, j, :] = self.map[i+1, j, :] + (learn_rate/2)*(x - self.map[i+1, j, :])
                if not(j == 0):
                    self.map[i, j-1, :] = self.map[i, j-1, :] + (learn_rate/2)*(x - self.map[i, j-1, :])
                if not(j == self.map.shape[1]-1):
                    self.map[i, j+1, :] = self.map[i, j+1, :] + (learn_rate/2)*(x - self.map[i, j+1, :])


    def select_classes(self, train_x, train_y):

        self.context_map = np.zeros(self.map.shape[:2]) - 1
        indexes = self.soft_predict(train_x)
        for index, y in zip(indexes, train_y):
            i, j = np.unravel_index(index, self.context_map.shape)
            self.context_map[i, j] = y
        
        # Caso algum neurônio ainda esteja sem rótulo
        # Rotular igual ao neurônio mais próximo
        index_indefinidos = np.where(self.context_map == -1)
        tries = 6
        while len(index_indefinidos[0]) and tries:
            tries = tries - 1
            print('Existem: ', len(index_indefinidos[0]), ' indexes indefinidos')
            for i, j in zip(*index_indefinidos):
                distancia_vizinho = np.array([float('inf'), float('inf'), float('inf'), float('inf')])
                if not(i == 0) and self.context_map[i-1, j] != -1:
                    distancia_vizinho[0] = np.sqrt(np.sum(np.power(self.map[i-1, j,:] - self.map[i, j, :], 2)))

                elif not(i == self.map.shape[0]-1) and self.context_map[i+1, j] != -1:
                    distancia_vizinho[1] = np.sqrt(np.sum(np.power(self.map[i+1, j,:] - self.map[i, j, :], 2)))

                elif not(j == 0) and self.context_map[i, j-1] != -1:
                    distancia_vizinho[2] = np.sqrt(np.sum(np.power(self.map[i, j-1,:] - self.map[i, j, :], 2)))

                elif not(j == self.map.shape[1]-1) and self.context_map[i, j+1] != -1:
                    distancia_vizinho[3] = np.sqrt(np.sum(np.power(self.map[i, j+1,:] - self.map[i, j, :], 2)))
                
                # Com certeza tem um jeito melhor
                # Mas estou sem tempo para pensar
                id = distancia_vizinho.argmin()
                if id == 0:
                    self.context_map[i, j] = self.context_map[i-1, j]
                if id == 1:
                    self.context_map[i, j] = self.context_map[i+1, j]
                if id == 2:
                    self.context_map[i, j] = self.context_map[i, j-1]
                if id == 3:
                    self.context_map[i, j] = self.context_map[i, j+1]
            index_indefinidos = np.where(self.context_map == -1)

        index_indefinidos = np.where(self.context_map == -1)
        print('Existem: ', len(index_indefinidos[0]), ' indexes indefinidos')



    # Precisa receber necessariamente uma matriz [entradas, features]
    # Não usa o mapa de contexto
    def soft_predict(self, test_x:np.ndarray):        
        distancias = np.sqrt(np.sum(np.power(self.map.reshape([1, -1, self.n_features]) - test_x.reshape([-1, 1, 3]), 2), axis=2))
        # print(distancias.argmin(axis=1).shape)
        # print(distancias.argmin(axis=1)[:10])
        # i, j = np.unravel_index(distancias.argmin(axis=0), distancias.shape)
        return distancias.argmin(axis=1)
    
    # Precisa receber necessariamente uma matriz [entradas, features]
    # Usa o mapa de contexto
    def predict(self, x:np.ndarray):
        i, j = np.unravel_index(self.soft_predict(x.reshape(1, -1)), self.context_map.shape)
        return self.context_map[i, j]

def import_data():
    treino = np.array(pd.read_csv('./datasets/exercicio_cap_8/treino.csv'))
    train_x = treino[:, :3]
    train_y = treino[:, 3]
    test_x = np.array(pd.read_csv('./datasets/exercicio_cap_8/teste.csv'))
    return train_x, train_y, test_x

if __name__ == "__main__":
    
    train_x, train_y, test_x = import_data()

    mapa = Kohonen(4, 4, 3)
    
    mapa.fit(train_x)

    mapa.select_classes(train_x, train_y)


    for i, x in enumerate(test_x):
        classe = mapa.predict(x)
        print(f'Amostra {x} Classe {classe}')

    print(mapa.context_map)









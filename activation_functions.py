import numpy as np
import matplotlib.pyplot as plt
import matplotlib as mpl


def degrau(x:np.ndarray):
    return np.where(x > 0, 1, -1)

def gen_simetrica(a:float):
    def simetrica(_x:np.ndarray):
        x = np.copy(_x)
        x[x > a] = a
        x[x < -a] = -a
        return x

    # Sei que ela não tem derivada, mas, se tivesse, seria algo assim
    def derivada(_x:np.ndarray):
        x = np.zeros_like(_x)
        x[np.logical_and(_x >-a, _x < a)] = 1
        return x

    return simetrica, derivada

def gen_logistica(beta:float):
    def logistica(x:np.ndarray):
        return 1/(1+np.exp(-beta*x))

    def derivada(x:np.ndarray):
        return -beta*np.exp(beta*x)/np.power(np.exp(beta*x) + 1, 2)
    return logistica, derivada

def gen_tanh(beta:float):
    def tanh(x:np.ndarray):
        return -np.expm1(-beta*x)/(1+np.exp(-beta*x))

    def derivada(x:np.ndarray):
        return 2*beta*np.exp(beta*x)/np.power((np.exp(beta*x) + 1), 2)
    return tanh, derivada

def linear(x:np.ndarray):
    return x

def linear_dx(x:np.ndarray):
    return 1

if __name__ == '__main__':
    np_test = np.array([-3, -2, -1, 0, 1, 2, 3])
    # v_degrau = np.vectorize(degrau)
    print('-- Teste degrau --')
    print(degrau(np_test))
    print(degrau(5))

    print('\n-- Teste simetrica --')
    simetrica1, _ = gen_simetrica(1)
    simetrica2, sim_dx = gen_simetrica(5)
    print(simetrica1(np_test))
    print(simetrica2(np_test))
    print(simetrica1(0))
    print(sim_dx(np_test))

    print('\n-- Teste logística --')
    logistica1, _ = gen_logistica(1)
    logistica2, logis_dx = gen_logistica(2)
    print(logistica1(np_test))
    print(logistica2(np_test))
    print(logis_dx(np_test))

    print('\n-- Teste tangente hiperbólica --')
    tanh1, _ = gen_tanh(1)
    tanh2, tanh_dx = gen_tanh(2)
    print(tanh1(np_test))
    print(tanh2(np_test))
    print(tanh1(np_test) - np.tanh(np_test))
    print(tanh2(np_test) - np.tanh(np_test))
    print(tanh_dx(np_test))
    



    font = {'family' : 'Times New Roman',
        'weight' : 'bold',
        'size'   : 18}
    mpl.rc('font', **font)
    plt.rcParams['figure.constrained_layout.use'] = True

    fig, axs = plt.subplots(2, 2)

    x = np.linspace(-10, 10, 50)

    axs[0,0].plot(x, degrau(x))
    axs[0,0].set_title('Degrau')

    axs[0,1].plot(x, simetrica2(x))
    axs[0,1].set_title('Simetrica')

    axs[1,0].plot(x, logistica1(x))
    axs[1,0].set_title('Logística')

    axs[1,1].plot(x, tanh1(x))
    axs[1,1].set_title('Tangente Hiperbólica')



    plt.show()
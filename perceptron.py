import numpy as np
from activation_functions import gen_tanh, degrau

# Tipo utilizado para indicar um tipo de função a ser recebido
# Ex: Callable[[argumentos da funcao], saída da funcao]
from collections.abc import Callable

def quadratic_error(predictions, y):
    return np.power(y - predictions, 2)

def quadratic_error_dx(predictions, y):
    return 2*(predictions - y)

def std_error(predictions, y):
    return predictions - y

class Perceptron:

    def __init__(self, input_size:int, activation_function:Callable[[float], float], derivative_act_function:Callable[[float], float],
    error=quadratic_error, error_dx=quadratic_error_dx):
        # Função random é de [0, 1]: *2 -1 vira [-1, 1]
        # self.weights = np.random.rand(input_size+1, 1)*2 -1
        self.weights = np.random.rand(input_size+1, 1)*0.1
        self.activation = activation_function
        self.activ_dx = derivative_act_function
        self.error = error
        self.error_dx = error_dx

    # Precisa necessariamente receber uma matriz de tamanho: [entradas, features]
    def predict(self, entry:np.ndarray):
        return self.activation(
            np.sum(
                np.matmul(np.concatenate((entry, np.ones([entry.shape[0], 1])), axis=1), self.weights), axis=1))

    # É um predict sem função de ativação
    def predict_adaline(self, entry:np.ndarray):
        return np.sum(np.matmul(np.concatenate((entry, np.ones([entry.shape[0], 1])), axis=1), self.weights), axis=1)

    def fit(self, n_epochs:int, learn_rate:float, train_x:np.ndarray, train_y:np.ndarray):
    
        train_fit = np.concatenate((train_x, np.ones([train_x.shape[0], 1])), axis=1)
        n_samples = len(train_y)
        erro_over_time = []
        for i in range(n_epochs):
            answers = self.predict(train_x)
            error_dx = self.error_dx(answers, train_y).reshape(-1, 1)
            answers_dx = self.activ_dx(answers)
            error = self.error(answers, train_y)
            erro_over_time.append(np.sum(np.abs(error)))
            if np.sum(np.abs(error)) < 0.00001:
                return n_epochs, erro_over_time
            if i % 100 == 0:
                print(f'Erro na época {i}: {np.sum(np.abs(error))}')

            # self.weights = (self.weights.T - np.sum(learn_rate*np.matmul(error.T,train_fit), axis=0)).T
            self.weights = (self.weights.T - (learn_rate)*np.sum(np.matmul((error_dx*answers_dx).T, train_fit), axis=0)).T
        return i, erro_over_time

    def fit_hebb(self, n_epochs:int, learn_rate:float, train_x:np.ndarray, train_y:np.ndarray):
        train_fit = np.concatenate((train_x, np.ones([train_x.shape[0], 1])), axis=1)
        n_samples = len(train_y)
        for i in range(n_epochs):

            answers = self.predict(train_x)
            error = self.error(answers, train_y).reshape(-1, 1)
            if i % 1000 == 0:
                print(f'Erro na época {i}: {np.sum(np.abs(error))}')
            if np.sum(np.abs(error)) == 0:
                break

            self.weights = (self.weights.T - np.sum(np.matmul(learn_rate*error.T,train_fit), axis=0)).T
        return i


    def fit_adaline(self, n_epochs:int, learn_rate:float, train_x:np.ndarray, train_y:np.ndarray):
        train_fit = np.concatenate((train_x, np.ones([train_x.shape[0], 1])), axis=1)

        n_samples = len(train_y)
        for i in range(n_epochs):
            answers = self.predict_adaline(train_x)
            error = self.error(answers, train_y).reshape(1, -1)
            error_dx = self.error_dx(answers, train_y).reshape(-1, 1)
            if i % 100 == 0:
                print(np.sum(np.abs(error)))

            # for x, y in zip(train_x, train_y):
            #     u = np.sum(x * self.weights)
            #     print(u.shape)
            #     input()

            self.weights = (self.weights.T - ((learn_rate/n_samples) * np.sum(np.matmul(error,train_fit), axis=0))).T
            

if __name__ == '__main__':

    activ, activ_dx = gen_tanh(1)
    neuron = Perceptron(2, activ, activ_dx)
    # print(neuron.weights)

    # print(neuron.predict(np.array([[1, 2]])))
    # print(neuron.predict(x))
    # print(neuron.predict(np.array([[1, 2], [3, 4]])))
    # print(np.array([[1, 2, 1], [3, 4, 1]]).shape)
    # print(np.array([[1, 2, 1], [3, 4, 1]]))

    # neuron.fit(20, 0.1, 
    #     np.array([[-1.5, -1.5], [1, 1], [2, 1], [-2, -4]]),
    #     np.array([-1, 1, 1, -1]))

    # print('\n', neuron.predict(np.array([[-1.5, -1.5], [1, 1], [2, 1], [-2, -4]])))

    h_neuron = Perceptron(2, degrau, None, std_error, None)
    h_neuron.fit_hebb(5, 0.1,
        np.array([[-1.5, -1.5], [1, 1], [2, 1], [-2, -4]]),
        np.array([-1, 1, 1, -1]))


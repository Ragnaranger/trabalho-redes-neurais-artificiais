from cmath import tanh
from perceptron import Perceptron, std_error, quadratic_error, quadratic_error_dx
from activation_functions import degrau, gen_tanh

import pandas as pd
import numpy as np

from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score

def experiment(df_pesos, df_teste, i):
    neuron = Perceptron(3, degrau, None, error=std_error, error_dx=None)

    start_weights = neuron.weights

    # Preparando dados
    train = np.array(pd.read_csv('./datasets/exercicio_cap_3/dataset_3.6.csv'))
    test = np.array(pd.read_csv('./datasets/exercicio_cap_3/dataset_test_3.6.csv'))


    epochs_trained = neuron.fit_hebb(3000, 0.001, train[:, :3], train[:, 3])
    final_weights = neuron.weights


    data=np.array([start_weights.reshape(-1), final_weights.reshape(-1)]).reshape(-1)
    erro_treinamento = np.sum(np.abs(neuron.error(neuron.predict(train[:, :3]), train[:, 3])))
    precisao = precision_score(train[:, 3], degrau(neuron.predict(train[:, :3])))
    # data = np.array([data, erro_treinamento, precisao])
    data = np.append(data, [erro_treinamento, precisao, epochs_trained]).reshape(1, -1)

    data = pd.DataFrame(data, columns=df_pesos.columns)
    df_pesos = df_pesos.append(data)
        
    output_test = neuron.predict(test).reshape(-1, 1)
    df_teste['yT'+str(i)] = output_test



    return df_pesos, df_teste

if __name__ == '__main__':
    # Iniciando Dataframe
    multi_index = pd.MultiIndex.from_product([['Vetor de Pesos Iniciais', 'Vetor de Pesos Finais'], ['w0','w1','w2','w3',]])
    df_pesos = pd.DataFrame(columns=multi_index)
    df_pesos['Erro Final Treinamento'] = ''
    df_pesos['Precisão'] = ''
    df_pesos['Número de épocas'] = ''

    # Dataframe de Teste
    df_teste = pd.read_csv('./datasets/exercicio_cap_3/dataset_test_3.6.csv')
    df_teste.columns.name = 'Amostra'

    # Executar o experimento 5 vezes
    for i in range(5):
        df_pesos, df_teste = experiment(df_pesos, df_teste, i)
    


    # Tabelas
    print(df_pesos)
    print(df_teste)
    df_pesos.to_latex('./output/df_pesos.tex', index=False)
    df_teste.to_latex('./output/df_teste.tex', index=False)
    df_pesos.to_csv('./output/df_pesos.csv', index=False)
    df_teste.to_csv('./output/df_teste.csv', index=False)

    
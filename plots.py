import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

import pandas as pd
import numpy as np

def plotplano():
    w = np.array([11.754396,  19.145462, -5.487472])
    b = np.array([23.045050])

    x = np.linspace(-1,2,10)
    y = np.linspace(-0.5,1,10)

    X, Y = np.meshgrid(x, y)
    Z = (-b - w[0]*X - w[1]*Y)/w[2]
    return X, Y, Z




# dataset = np.array(pd.read_csv('./datasets/exercicio_cap_3/dataset_3.6.csv'))

dataset = np.array(pd.read_csv('datasets/prova1.1/treino.csv'))
test_set = np.array(pd.read_csv('datasets/prova1.1/teste.csv'))
test_ans = [-1, -1, -1,  1,  1, -1,  1,  1, -1, -1]


# dataset = np.array(pd.read_csv('datasets/prova1.2/train.csv'))
# test_set = np.array(pd.read_csv('datasets/prova1.2/teste.csv'))
# test_ans = [-1, -1,  1,  1,  1,  1, -1, -1,  1, -1]


for test_entry, test_ans in zip(test_set, test_ans):

    print(test_entry)
    print(test_entry.shape)
    print(test_ans)
    

    fig = plt.figure()
    ax = fig.add_subplot(111, projection='3d')
    
    class_a = dataset[dataset[:, 3] == 1]
    class_b = dataset[dataset[:, 3] == -1]

    if test_ans == 1:
        ax.scatter(class_a[:, 0], class_a[:, 1], class_a[:, 2], c='r', alpha=0.01)
    else:
        ax.scatter(class_b[:, 0], class_b[:, 1], class_b[:, 2], alpha=0.01)

    ax.scatter(test_entry[0], test_entry[1], test_entry[2], c='g')

    ax.set_xlabel('x1')
    ax.set_ylabel('x2')
    ax.set_zlabel('x3')

    # w = np.array([11.754396,  19.145462, -5.487472])
    # b = np.array([23.045050])

    # X, Y, Z = plotplano()
    # ax.plot_surface(X, Y, Z, color='green', alpha=0.5)
    # ax.plot_trisurf(x, y, z, color='green', alpha=0.5)

    plt.show()
    input()
from network import Network

import pandas as pd
import numpy as np

from sklearn.utils import shuffle
from sklearn.model_selection import train_test_split
from sklearn.metrics import confusion_matrix, f1_score

import matplotlib.pyplot as plt

def import_data():
    data_no_attack = np.array(pd.read_csv('./../datasets/orion/data_no_attack_norm.csv'))
    data_attack_1 = np.array(pd.read_csv('./../datasets/orion/data_attack_1_norm.csv'))
    data_attack_2 = np.array(pd.read_csv('./../datasets/orion/data_attack_2_norm.csv'))

    return data_no_attack, data_attack_1, data_attack_2


def create_generator_loss_function(discriminador:Network):
    # Nesse caso y_true deve ser um np.zeros(prediction.shape[0]),
    # Já que o objetivo é gerar exemplos que o discriminador ache normal
    def generator_loss_function(prediction:np.ndarray, y_true:np.ndarray):
        discriminator_prediction = discriminador.predict(prediction)
        # Adicionando um pouco de ruído para evitar um overfit para um único valor
        return (discriminator_prediction - y_true)**2 + np.random.random(y_true.shape)*0.01
    
    return generator_loss_function


if __name__ == "__main__":
    # Lembrando que 0 é um tráfego normal
    # E 1 é tráfego anômalo...

    data_no_attack, data_attack_1, data_attack_2 = import_data()

    # Recebe valores aleatórios, gera dimensões de fluxo
    gerador = Network(12, 6, 'Gerador')
    gerador.add_layer(12, 'sigmoid')
    gerador.add_layer(12, 'sigmoid')
    gerador.last_layer('sigmoid')

    # Recebe dimensões de fluxo, diz se são reais ou não
    discriminador = Network(6, 1, 'Discriminador')
    discriminador.add_layer(12, 'sigmoid')
    discriminador.add_layer(12, 'sigmoid')
    discriminador.last_layer('sigmoid')




    generator_loss_function = create_generator_loss_function(discriminador)
    # Vou fazer algumas iterações de treinamento alternadas
    for i in range(5):
        input_gerador = np.random.random([86400, 12])
        exemplos_falsos = gerador.predict(input_gerador)

        # Juntar exemplos verdadeiros e falsos
        x_train = np.concatenate([data_no_attack[:, :6], exemplos_falsos], axis=0)
        y_train = np.concatenate([data_no_attack[:, 6], np.ones(exemplos_falsos.shape[0])], axis=0)
        # Embaralhar e separar em treino e validação
        x_train, x_val, y_train, y_val = train_test_split(x_train, y_train.reshape((-1,1)), test_size=0.2, random_state=42+i)

        x_train_gen, x_val_gen, y_train_gen, y_val_gen = train_test_split(input_gerador, np.zeros((input_gerador.shape[0], 1)), test_size=0.2, random_state=42+i)

        print('\n\nTreinando Discriminador...')
        discriminador.train(x_train, y_train, x_val, y_val, n_epochs=1000, decay=0)
        print('\nTreinando Gerador...')
        gerador.train_gan(x_train_gen, y_train_gen, x_val_gen,  y_val_gen, discriminador, n_epochs=800, decay=0, loss=generator_loss_function)

        # Testar discriminador
        fix_previsao = np.where(discriminador.predict(data_attack_1[:, :6]) > 0.5, 1, 0)
        print(confusion_matrix(data_attack_1[:, 6], fix_previsao))
        print(f1_score(data_attack_1[:, 6], fix_previsao))

        fig, ax = plt.subplots()
        previsao_disc = discriminador.predict(data_attack_1[:, :6])
        x_axis = np.arange(previsao_disc.shape[0])

        x_axis_attack = x_axis[data_attack_1[:, 6] == 1]
        y_axis_attack = previsao_disc[data_attack_1[:, 6] == 1]

        ax.plot(np.arange(previsao_disc.shape[0]), previsao_disc, color='b')
        ax.plot(x_axis_attack, y_axis_attack, color='r')

        # Mostrando gerador
        previsao_gerador = gerador.predict(input_gerador)
        fig, axs = plt.subplots(3, 2)
        for i, ax in enumerate(axs.flatten()):
            ax.plot(np.arange(input_gerador.shape[0]), previsao_gerador[:, i])
            ax.plot(np.arange(data_no_attack.shape[0]), data_no_attack[:, i])

        print()
        print()

    plt.show()




        
    

from network import Network
from utils import *

from subject import Subject, Observer

import matplotlib.pyplot as plt
import matplotlib as mpl
import numpy as np
import pandas as pd

from sklearn.model_selection import train_test_split
from sklearn.metrics import mean_squared_error

class Val_error_list(Observer):
    def __init__(self):
        self.__list = []


    def update(self, subject: Subject):
        self.__list.append(subject.mean_squared_error)

    def get_list(self):
        return self.__list

class Train_error_list(Observer):
    def __init__(self):
        self.__list = []


    def update(self, subject: Subject):
        self.__list.append(subject.train_error)
        # print(self.__list)

    def get_list(self):
        return self.__list

class Accuracy(Observer):
    def __init__(self, train_x, train_y, val_x, val_y):
        self.train_x = train_x
        self.train_y = train_y
        self.val_x = val_x
        self.val_y = val_y
        self.__train_accuracy = []
        self.__val_accuracy = []
    
    def update(self, subject: Subject):

        train_acc = get_accuracy(
            fix(subject.predict(self.train_x)),
            self.train_y
        )

        val_acc = get_accuracy(
            fix(subject.predict(self.val_x)), 
            self.val_y
        )


        self.__train_accuracy.append(train_acc)
        self.__val_accuracy.append(val_acc)
    
    def get_train(self):
        return self.__train_accuracy
    
    def get_val(self):
        return self.__val_accuracy

def plot_error_graph(val_error_list, train_error_list, step):
    plt.rcParams['figure.constrained_layout.use'] = True
    font = {'family' : 'Times New Roman',
        'weight' : 'bold',
        'size'   : 18}
    mpl.rc('font', **font)

    y_val = np.array(val_error_list)
    y_train = np.array(train_error_list)

    xlim = step * len(val_error_list)
    x = np.arange(0, xlim, step)


    plt.xlim((0, xlim + step))
    plt.ylim((0, np.max(y_val) + 0.02))

    plt.plot(x, y_val, color='r')
    plt.plot(x, y_train, color='b')
    plt.legend(['Erro Validação', 'Erro Treino'], loc='upper right')
    plt.show()

def plot_acc_graph(val_acc_list, train_acc_list, step):
    plt.rcParams['figure.constrained_layout.use'] = True
    font = {'family' : 'Times New Roman',
        'weight' : 'bold',
        'size'   : 18}
    mpl.rc('font', **font)

    y_val = np.array(val_acc_list)
    y_train = np.array(train_acc_list)
    xlim = step * len(val_acc_list)
    x = np.arange(0, xlim, step)


    plt.xlim((0, xlim + step))
    plt.ylim((0.05, 1.05))

    plt.plot(x, y_val, color='r')
    plt.plot(x, y_train, color='b')
    plt.legend(['Acurácia Treino', 'Acurácia Validação'], loc='lower right')
    plt.ylabel('Acurácia')
    plt.xlabel('Número de épocas x100')
    
    plt.show()

def load_dataset():
    train = np.array(pd.read_csv('./datasets/exercicio_cap_5/treino5.9.csv'))
    train_x = train[:, :4]
    train_y = train[:, 4:]
    train_x ,val_x, train_y, val_y = train_test_split(train_x, train_y, test_size=0.2,shuffle=True ,random_state=44)

    test = np.array(pd.read_csv('./datasets/exercicio_cap_5/teste5.9.csv'))
    test_x = test[:, :4]
    test_y = test[:, 4:]
    

    return train_x, train_y, val_x, val_y, test_x, test_y

if __name__ == "__main__":
    # train_x, train_y, val_x, val_y, test_x, test_y = prepare_data('.\\base_data\MNIST_normalized.csv', 784, ',', export_to='.\data\\novo_MNIST_normalized')
    # train_x, train_y, val_x, val_y, test_x, test_y = load_data('.\data\\novo_MNIST_normalized', 784)
    plt.rcParams['figure.constrained_layout.use'] = True

    train_x, train_y, val_x, val_y, test_x, test_y = load_dataset()

    df = pd.read_csv('./datasets/exercicio_cap_5/teste5.9.csv')
    # df_erro = pd.DataFrame(['T1','T2','T3','T4','T5'], columns=['Treinamento'])
    # df_erro['Erro Quadrático Médio'] = 0
    # df_erro['Número total de épocas'] = 0


    # model = Network.load_network('./final/rede2.pkl')
    model = Network(4, 3, name='exercicio_5.9')
    model.add_layer(15, activation_function='sigmoid')
    model.last_layer(activation_function='sigmoid')


    val_error_list = Val_error_list()
    model.subscribe(val_error_list, 'on_100_epochs')
    train_error_list = Train_error_list()
    model.subscribe(train_error_list, 'on_100_epochs')
    acc_list = Accuracy(train_x, train_y, val_x, val_y)
    model.subscribe(acc_list, 'on_100_epochs')

    model.view()

    model.train(train_x, train_y, val_x, val_y, learn_rate=0.1, n_epochs=10000, decay=0.000000001)
    

    model.save_network('./output/redes_trabalho')
    

    ans = model.predict(test_x)
    mse = mean_squared_error(test_y, ans)
    print('Erro Quadrático Médio', mse)
    ans = fix(ans)
    check_result(ans, test_y)

    df['y0'] = ans[:, 0]
    df['y1'] = ans[:, 1]
    df['y2'] = ans[:, 2]


    # df['T'+str(i)] = ans
    # df_erro.at[i, 'Erro Quadrático Médio'] = erro
    # df_erro.at[i, 'Número total de épocas'] = 10000
    # print(df)
    # print(df_erro)
    # print(ans[:3])

    plot_acc_graph(acc_list.get_val(), acc_list.get_train(), 1)
    plot_error_graph(val_error_list.get_list(), train_error_list.get_list(), 1)

    print(df)
    df.to_latex('./output/df_ex5.9_3.tex', index=False)
    # print(df_erro)
    # df_erro.to_latex('./output/df_ex5.9_2.tex', index=False)

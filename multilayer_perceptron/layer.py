import numpy as np

class Layer:
    def __init__(self, n_neurons, n_entrys, activation_function):
        self.n_neurons = n_neurons
        self.activation_function_name = activation_function
        self.activation_function = functions[activation_function]
        self.d_activation_function = derivatives[activation_function]

        self.best_weights = None
        self.best_bias = None

        self.weights = np.random.random((n_neurons, n_entrys)) - 0.5
        self.bias = np.random.random(n_neurons) - 0.5

    def predict(self, data):
        return self.activation_function(np.matmul(data, self.weights.T) + self.bias)



def sigmoid(x):
    return 1 / (1 + np.exp(-x))

def d_sigmoid(x):
    return x * (1 - x)

def relu(x):
    return np.maximum(0, x)

def d_relu(_x):
    return np.where(_x<=0, 0, 1)
    x = _x.copy()
    x[x <= 0] = 0
    x[x >  0] = 1
    return x

def tanh(x):
    return (np.exp(x)-np.exp(-x))/(np.exp(x)+np.exp(-x))

def d_tanh(x):
    return 1 - tanh(x)**2

functions = {'sigmoid':sigmoid, 'relu':relu, 'tanh':tanh}
derivatives = {'sigmoid':d_sigmoid, 'relu':d_relu, 'tanh':d_tanh}
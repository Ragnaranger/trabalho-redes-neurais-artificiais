import numpy as np
from tensorflow.keras.datasets import mnist
from math import floor, ceil


def import_from_keras():

    (X_train, Y_train), (X_test, Y_test) = mnist.load_data()
    X_train = X_train.reshape((60000, -1))
    Y_train = Y_train.reshape((60000, 1))
    train = np.concatenate((X_train,Y_train), axis=1)


    X_test = X_test.reshape((10000, -1))
    Y_test = Y_test.reshape((10000, 1))
    test = np.concatenate((X_test,Y_test), axis=1)

    dataset = np.concatenate((train, test), axis=0)


    _class = dataset[dataset[:, -1] == 0]
    class_len = len(_class)
    _class = np.delete(_class,-1, axis=1)
    new = np.zeros((class_len, 10))
    new[:, 0] = 1

    _class = np.append(_class, new, axis=1)


    # Embaralhar
    np.random.shuffle(_class)

    # Separar conjuntos de treinamento, validação e teste
    train_len = floor(class_len * 0.8)             # 80%
    val_len = ceil(ceil(class_len * 0.8) * 0.125)  # 10%
    test_len = class_len - (train_len + val_len)   # 10%

    train = _class[:ceil(train_len)]
    val = _class[ceil(train_len) : ceil(train_len) + ceil(val_len)]
    test = _class[ceil(train_len) + ceil(val_len):]


    for i in range(1, 10):
        _class = dataset[dataset[:, -1] == i]

        class_len = len(_class)
        _class = np.delete(_class,-1, axis=1)
        new = np.zeros((class_len, 10))
        new[:, i] = 1

        _class = np.append(_class, new, axis=1)

        train_len = floor(class_len * 0.8)             # 80%
        val_len = ceil(ceil(class_len * 0.8) * 0.125)  # 10%
        test_len = class_len - (train_len + val_len)   # 10%

        np.random.shuffle(_class)

        train = np.append(train, _class[:train_len], axis=0)
        val = np.append(val, _class[train_len : train_len + val_len], axis=0)
        test = np.append(test, _class[train_len + val_len:], axis=0)

    # Embaralhar após separação
    np.random.shuffle(train)
    np.random.shuffle(val)
    np.random.shuffle(test)


    # Separar x e y
    train_x,    train_y   =  train[:, :784], train[:, 784:]
    val_x,      val_y     =  val[:, :784],   val[:, 784:]
    test_x,     test_y    =  test[:, :784],  test[:, 784:]


    return train_x, train_y, val_x, val_y, test_x, test_y
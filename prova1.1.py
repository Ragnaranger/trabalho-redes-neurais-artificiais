import pandas as pd
import numpy as np

from activation_functions import degrau, gen_tanh
from perceptron import Perceptron, std_error

from sklearn.metrics import precision_score

def import_data():
    train = np.array(pd.read_csv('datasets/prova1.1/treino.csv'))
    train_x = train[:, :3]
    train_y = train[:, 3]

    test_x = np.array(pd.read_csv('datasets/prova1.1/teste.csv'))

    return train_x, train_y, test_x



if __name__ == '__main__':


    train_x, train_y, test_x = import_data()

    tanh, tanh_dx = gen_tanh(1)
    model = Perceptron(3, degrau, None, error=std_error, error_dx=None)

    model.fit_hebb(10000, 0.01, train_x, train_y)

    prediction = model.predict(train_x)

    print(precision_score(train_y, prediction))

    print(model.predict(test_x))

    # Respostas retornadas:
    #
    # [-1 -1 -1  1  1 -1  1  1 -1 -1]
    # [-1 -1 -1  1  1 -1  1  1 -1 -1]




